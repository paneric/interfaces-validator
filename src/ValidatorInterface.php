<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Validator;

interface ValidatorInterface
{
    public function setMessages(array $rules, array $values, array $attributes = []): array;

    public function setReport(array $report): void;

    public function getReport(): ?array;
}
